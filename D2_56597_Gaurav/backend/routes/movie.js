const express = require('express')
const db = require('../db')
const utils = require('../utils')
const router = express.Router()

//GET FUNCTIONALITY TO DISPLAY MOVIE
router.get('/get', (request, response) => {
    const query = `SELECT movie_id, movie_title, movie_release_date,movie_time,director_name
    FROM Movie`
    db.execute(query, (error, result ) => {
        response.send(utils.createResult(error, result))
    })
})

//Post or Add Functionality to add movie
router.post('/add', (request, response) => {
    const {movie_id, movie_title, movie_release_date,movie_time,director_name} = request.body
    const query = `INSERT INTO Movie (movie_id, movie_title, movie_release_date,movie_time,director_name) 
    VALUES ('${movie_id}', '${movie_title}', '${movie_release_date}','${movie_time}','${director_name}')`
    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

//Update functionality
router.put('/:movie_id', (request, response) => {
    const {movie_id} = request.params
    const {movie_title, movie_release_date,movie_time,director_name} = request.body
    const query = `UPDATE Movie SET
    movie_title = '${movie_title}', 
    movie_release_date = '${movie_release_date}',
    movie_time = '${movie_time}',
    director_name = '${director_name}'
    WHERE movie_id = ${movie_id} `
    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

//Delete
router.delete('/:movie_id', (request, response) => {
    const {movie_id} = request.params
    const query = `DELETE FROM Movie WHERE 
    movie_id = ${movie_id} `
    db.execute(query, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

module.exports = router