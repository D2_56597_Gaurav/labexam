CREATE TABLE Movie (
    movie_id INTEGER PRIMARY KEY, 
    movie_title VARCHAR (100), 
    movie_release_date DATE,
    movie_time VARCHAR (100),
    director_name VARCHAR (100));